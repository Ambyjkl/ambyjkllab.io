const path = require('path')

const pkg = require('./package')

const png = rule => rule.test.test('.png')

const config = {
  /*
  ** Headers of the page
  */
  // build: {
  //   analyze: true,
  //   // or
  //   // analyze: {
  //   //   analyzerMode: 'static'
  //   // }
  // },
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    title: 'Amby Balaji',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'canonical', href: 'https://ambyjkl.tech' },
    ],
  },
  css: [
    'typeface-fira-sans',
    'assets/css/styles.scss',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#FFFFFF' },
  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public',
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      const urlLoader = config.module.rules.find(png)
      urlLoader.test = /\.(png|jpe?g|gif|webp)$/

      config.module.rules.push({
        test: /\.svg$/,
        resourceQuery: /inline/,
        loader: 'vue-svg-loader',
      }, {
        test: /\.svg$/,
        resourceQuery: /url/,
        loader: 'svg-url-loader',
        options: {
          noquotes: true,
          stripdeclarations: true,
        }
      })
    },
    babel: {
      presets: [['@vue/babel-preset-app', {
        targets: {
          browsers: pkg.browserslist,
        },
        useBuiltIns: 'usage',
        loose: true,
      }]],
    },
  },
  modules: [
    // Simple usage
    ['@nuxtjs/google-analytics', {
      id: 'UA-90754770-1'
    }]
  ],
}

if (process.env.NODE_ENV === 'production') {
  config.build.postcss = [
    require('@fullhuman/postcss-purgecss')({
      content: [
        './pages/**/*.vue',
        './components/**/*.vue',
        './asset/**/.css',
        './asset/**/.scss',
      ],
      whitelist: ['html', 'body'],
    })
  ]
}

module.exports = config
